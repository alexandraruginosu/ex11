package com.company;

public class TestGeneric {

    public static void main(String[] args) {
	// write your code here
        Generic <String> w1 = new Generic <String>("Samuel Jackson");
        Generic <String> w2 = new Generic <String>("Plaza Hotel");
        Generic <Integer> w3 = new Generic <Integer>(2000);

        System.out.println("My name is " + w1.getWeird());
        System.out.println("I work as a cashier at " + w2.getWeird());
        System.out.println("My salary is valued somewhere around " + w3.getWeird() + " dollars");
    }
}
